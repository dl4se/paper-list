# Papers on Deep Learning for Source Code
### Contents

* [1. Survey Papers](#1-survey-papers)
* [2. Model](#2-model) 
* [3. Adversarial Attack](#3-attack-attack) 
* [4. Testing](#4-testing) 
* [5. Poisoning](#5-poisoning)
* [6. Evaluation](#6-evaluation) 
* [7. Dataset](#7-dataset) 
* [8. Others](#8-others) 

## 1. Survey Papers

1. **Deep Learning & Software Engineering: State of Research and Future Directions**. NSF Workshop. [[pdf](https://arxiv.org/pdf/2009.08525.pdf)] [[code](https://gitlab.com/dlse-workshop/dlse-workshop-community-report?utm_source=catalyzex.com)]
2. **Deep learning for source code modeling and generation: Models, applications, and challenges**. ACM Computing Surveys 2020. [[pdf](https://dl.acm.org/doi/abs/10.1145/3383458)]
3. **Advances in Code Summarization**. ICSE-Companion 2021. [[pdf](https://ieeexplore.ieee.org/abstract/document/9402289)]
4. **A Survey on Machine Learning Techniques for Source Code Analysis**, arXiv 2021. [[pdf](https://arxiv.org/abs/2110.09610)]


## 2. Model

1. **CodeXGLUE: A Machine Learning Benchmark Dataset for Code Understanding and Generation**. [[pdf](https://arxiv.org/abs/2102.04664)] [[code](https://github.com/microsoft/CodeXGLUE)]
2. **CodeTrans: Towards Cracking the Language of Silicon's Code Through Self-Supervised Deep Learning and High Performance Computing**. [[pdf](https://arxiv.org/abs/2104.02443)] [[code](https://github.com/agemagician/CodeTrans)]
3. **CodeBERT: A Pre-Trained Model for Programming and Natural Languages**. EMNLP 2020. [[pdf](https://arxiv.org/pdf/2002.08155.pdf)] [[code](https://github.com/microsoft/CodeBERT)]
4. **A Transformer-based Approach for Source Code Summarization**. ACL 2020. [[pdf](https://arxiv.org/pdf/2005.00653.pdf)] [[code](https://github.com/wasiahmad/NeuralCodeSum)]
5. **PLBART: Unified Pre-training for Program Understanding and Generation**. NAACL 2021. [[pdf](https://arxiv.org/pdf/2103.06333.pdf)] [[code](https://github.com/wasiahmad/PLBART)]
6. **Language-agnostic representation learning of source code from structure and context**. ICLR 2021. [[pdf](https://arxiv.org/pdf/2103.11318.pdf)] [[code](https://github.com/danielzuegner/code-transformer)]
7. **CodeT5: Identifier-aware Unified Pre-trained Encoder-Decoder Models for Code Understanding and Generation**. EMNLP 2021. [[pdf](https://arxiv.org/abs/2109.00859)] [[code](https://github.com/salesforce/CodeT5)]
8. **UniXcoder: Unified Cross-Modal Pre-training for Code Representation**, ACL 2022. [[pdf](https://arxiv.org/abs/2203.03850)]

## 3. Adversarial Attack

1. **Misleading authorship attribution of source code using adversarial learning**, USENIX Security 19.[[pdf](https://www.usenix.org/conference/usenixsecurity19/presentation/quiring)]
2. **Adversarial Examples for Models of Code**, OOPSLA 2020. [[pdf](https://arxiv.org/abs/1910.07517)] 
3. **Generating Adversarial Examples for Holding Robustness of Source Code Processing Models**, AAAI 2020. [[pdf](https://ojs.aaai.org/index.php/AAAI/article/view/5469)]
4. **Adversarial robustness for code**, ICML 2020. [[pdf](https://proceedings.mlr.press/v119/bielik20a.html)]
5. **Generating Adversarial Computer Programs using Optimized Obfuscations**, ICLR 2021. [[pdf](https://arxiv.org/pdf/2103.11882.pdf)]
6. **Generating Adversarial Examples of Source Code Classification Models via Q-Learning-Based Markov Decision Process**, QRS 2021. [[pdf](https://ieeexplore.ieee.org/abstract/document/9724884)]
7. **A Practical Black-Box Attack on Source Code Authorship Identification Classifiers**, TIFS 2021. [[pdf](https://ieeexplore.ieee.org/abstract/document/9454564)]
8. **Code Summarization: Do Transformers Really Understand Code?** ICLR [[Workshop](https://openreview.net/group?id=ICLR.cc/2022/Workshop/DL4C)] 2022 [[pdf](https://openreview.net/pdf?id=rI5ll2_-1Zc)]
9. **Generating Adversarial Source Programs Using Important Tokens-based Structural Transformations**, ICECCS 2022. [[pdf](https://ieeexplore.ieee.org/abstract/document/9763729)]
10. **Natural Attack for Pre-trained Models of Code**, ICSE 2022. [[pdf](https://arxiv.org/abs/2201.08698)]
11. **RoPGen: Towards Robust Code Authorship Attribution via Automatic Coding Style Transformation**, ICSE 2022. [[pdf](https://arxiv.org/abs/2202.06043)]
12. **Towards Robustness of Deep Program Processing Models—Detection, Estimation, and Enhancement**, TOSEM 2022. [[pdf](https://dl.acm.org/doi/full/10.1145/3511887)]


## 4. Testing
1. **Assessing Robustness of ML-Based Program Analysis Tools using Metamorphic Program Transformations**. ASE-NIER Track 2021

## 5. Poisoning
1. **You Autocomplete Me: Poisoning Vulnerabilities in Neural Code Completion**. USENIX Security 21.[[pdf](https://www.usenix.org/conference/usenixsecurity21/presentation/schuster)]


## 6. Evaluation

1. **Evaluating Robustness to Input Perturbations
for Neural Machine Translation**. [[pdf](https://arxiv.org/pdf/2005.00580.pdf)] 

## 7. Dataset
1. **CoDesc: A Large Code-Description Parallel Dataset**. ACL 2021. [[pdf](https://arxiv.org/abs/2105.14220)] [[code](https://github.com/csebuetnlp/CoDesc)]

## 8. Others
1. **A Simple Approach for Handling Out-of-Vocabulary Identifiers in Deep Learning for Source Code**. NAACL 2021. [[pdf](https://arxiv.org/abs/2010.12663)] [[code](https://github.com/bayesgroup/code_transformers)]

